package library.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * Book, with information about it's title, isbn, author and total number of
 * items in library
 * 
 * @author huginroijmans
 */
@Entity
public class Book {

	@Id
	@GeneratedValue
	private Long id;

	@NotBlank(message = "Title is mandatory")
	private String title;

	@NotBlank(message = "ISBN is mandatory")
	private String isbn;

	@NotBlank(message = "Author is mandatory")
	private String author;

	private int nrOfItems = 1;

	Book() {
	}

	public Book(String title, String isbn, String author, int nrOfItems) {
		this.setTitle(title);
		this.setIsbn(isbn);
		this.setAuthor(author);
		this.setNrOfItems(nrOfItems);
	}

	public Long getId() {
		return this.id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getNrOfItems() {
		return nrOfItems;
	}

	public void setNrOfItems(int nrOfItems) {
		this.nrOfItems = nrOfItems;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Book))
			return false;
		Book other = (Book) o;
		return Objects.equals(this.id, other.id) && Objects.equals(this.getTitle(), other.getTitle())
				&& Objects.equals(this.getIsbn(), other.getIsbn())
				&& Objects.equals(this.getAuthor(), other.getAuthor())
				&& Objects.equals(this.getNrOfItems(), other.getNrOfItems());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.getTitle(), this.getIsbn(), this.getAuthor(), this.getNrOfItems());
	}

	@Override
	public String toString() {
		return "Book{" + "id=" + this.id + ", title='" + this.getTitle() + '\'' + ", isbn='" + this.getIsbn() + '\''
				+ ", author='" + this.getAuthor() + '\'' + ", nrOfItems='" + this.getNrOfItems() + '\'' + '}';
	}
}