package library.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * Member of the library who can borrow {@link Book}s.
 * 
 * @author huginroijmans
 */
@Entity
public class Member {
	@Id
	@GeneratedValue
	private Long id;

	@NotBlank(message = "Name is mandatory")
	private String name;

	Member() {
	}

	public Member(String name) {
		this.setName(name);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Member))
			return false;
		Member other = (Member) o;
		return Objects.equals(this.id, other.id) && Objects.equals(this.getName(), other.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.getName());
	}

	@Override
	public String toString() {
		return "Member{" + "id=" + this.id + ", name='" + this.getName() + '\'' + '}';
	}
}
