package library.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * A Loan is a {@link Member} borrowing a {@link Book} of the library.
 * 
 * @author huginroijmans
 */
@Entity
public class Loan {
	@Id
	@GeneratedValue
	private Long id;

	@NotNull(message = "Book is mandatory")
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "book_id", referencedColumnName = "id")
	private Book book;

	@NotNull(message = "Member is mandatory")
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "member_id", referencedColumnName = "id")
	private Member member;

	private Date date;

	Loan() {
	}

	public Loan(Book book, Member member, Date date) {
		this.setBook(book);
		this.setMember(member);
		this.setDate(date);
	}

	public Long getId() {
		return id;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Loan))
			return false;
		Loan other = (Loan) o;
		return Objects.equals(this.id, other.id) && Objects.equals(this.getBook(), other.getBook())
				&& Objects.equals(this.getMember(), other.getMember())
				&& Objects.equals(this.getDate(), other.getDate());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.getBook(), this.getMember(), this.getDate());
	}

	@Override
	public String toString() {
		return "Loan{" + "id=" + this.id + ", book='" + this.getBook().getId() + '\'' + ", member='"
				+ this.getMember().getId() + '\'' + ", date='" + this.getDate() + '\'' + '}';
	}
}
