package library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import library.model.Member;

/**
 * Repository for loading and saving {@link Member}s.
 * 
 * @author huginroijmans
 */
public interface MemberRepository extends JpaRepository<Member, Long> {

}