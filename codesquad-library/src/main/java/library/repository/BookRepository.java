package library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import library.model.Book;

/**
 * Repository for loading and saving {@link Book}s.
 * 
 * @author huginroijmans
 */
public interface BookRepository extends JpaRepository<Book, Long> {

}