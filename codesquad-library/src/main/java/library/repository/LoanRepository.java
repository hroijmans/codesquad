package library.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import library.model.Loan;

/**
 * Repository for loading and saving {@link Loan}s.
 * 
 * @author huginroijmans
 */
public interface LoanRepository extends JpaRepository<Loan, Long> {

	@Query("SELECT l FROM Loan l WHERE l.member.id = ?1")
	List<Loan> findByMember(long memberId);

	@Query("SELECT l FROM Loan l WHERE l.book.id = ?1")
	List<Loan> findByBook(long bookId);
}