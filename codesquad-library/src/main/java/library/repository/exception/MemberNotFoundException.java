package library.repository.exception;

public class MemberNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -6695897572655054216L;

	public MemberNotFoundException(Long id) {
		super("Could not find member " + id);
	}
}