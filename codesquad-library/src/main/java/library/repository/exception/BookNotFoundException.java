package library.repository.exception;

public class BookNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 8851537701600620187L;

	public BookNotFoundException(Long id) {
		super("Could not find book " + id);
	}
}