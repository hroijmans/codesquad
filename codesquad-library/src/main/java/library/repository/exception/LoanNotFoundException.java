package library.repository.exception;

public class LoanNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 7106076553642861329L;

	public LoanNotFoundException(Long id) {
		super("Could not find loan " + id);
	}
}