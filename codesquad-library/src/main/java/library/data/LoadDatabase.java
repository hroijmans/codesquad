package library.data;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import library.model.Book;
import library.model.Loan;
import library.model.Member;
import library.repository.BookRepository;
import library.repository.LoanRepository;
import library.repository.MemberRepository;

/**
 * Initialize (test) dataset.
 * 
 * @author huginroijmans
 */
@Configuration
class LoadDatabase {
	private static final Logger LOG = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	CommandLineRunner initDatabase(BookRepository bookRepo, MemberRepository memberRepo, LoanRepository loanRepo) {

		return args -> {
			Book roodKapje = new Book("Roodkapje", "1237626733", "De Wolf", 10);
			LOG.info("Preloading " + bookRepo.save(roodKapje));
			LOG.info("Preloading " + bookRepo.save(new Book("Sneeuwwitje", "3412576323", "De Jager", 5)));
			LOG.info("Preloading " + bookRepo.save(new Book("Hans en Grietje", "3445643634", "De heks", 12)));

			Member hugin = new Member("Hugin Roijmans");
			LOG.info("Preloading " + memberRepo.save(hugin));
			LOG.info("Preloading " + memberRepo.save(new Member("Klaas Vaak")));

			LOG.info("Preloading " + loanRepo.save(new Loan(roodKapje, hugin, new Date())));
		};
	}
}