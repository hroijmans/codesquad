package library.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import library.controller.dto.LoanAddDto;
import library.model.Book;
import library.model.Loan;
import library.model.Member;
import library.repository.BookRepository;
import library.repository.LoanRepository;
import library.repository.MemberRepository;

@Service()
public class LoanService {

	@Autowired
	private LoanRepository loanRepo;
	@Autowired
	private BookRepository bookRepo;
	@Autowired
	private MemberRepository memberRepo;

	public Loan createNew(LoanAddDto newLoanDto) {
		List<Loan> loansByMember = loanRepo.findByMember(newLoanDto.getMember());
		if (loansByMember.size() >= 7) {
			// TODO pass error message through
			throw new IllegalArgumentException("Maximum 7 books per member allowed");
		}
		Book book = bookRepo.getOne(newLoanDto.getBook());
		if (loansByMember.stream().anyMatch(l -> l.getBook().equals(book))) {
			// TODO pass error message through
			throw new IllegalArgumentException("Book already borrowed by this member");
		}
		List<Loan> loansByBook = loanRepo.findByBook(newLoanDto.getBook());
		if (loansByBook.size() >= book.getNrOfItems()) {
			// TODO pass error message through
			throw new IllegalArgumentException("All books have been lend out");
		}
		Member member = memberRepo.getOne(newLoanDto.getMember());
		return loanRepo.save(new Loan(book, member, new Date()));
	}
}