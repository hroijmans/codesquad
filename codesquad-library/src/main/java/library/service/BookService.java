package library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import library.repository.BookRepository;
import library.repository.LoanRepository;

@Service()
public class BookService {

	@Autowired
	private LoanRepository loanRepo;
	@Autowired
	private BookRepository bookRepo;

	public void deleteBook(long id) {
		if (!loanRepo.findByBook(id).isEmpty()) {
			// TODO pass error message through
			throw new IllegalArgumentException("Books cannot be deleted while borrowed by 1 or more members");
		}
		bookRepo.deleteById(id);
	}
}