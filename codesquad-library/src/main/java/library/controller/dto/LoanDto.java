package library.controller.dto;

import java.util.Date;

import library.model.Loan;

public class LoanDto {

	private final long id;
	private final String title;
	private final String isbn;
	private final String member;
	private final Date date;

	public LoanDto(Loan loan) {
		this.id = loan.getId();
		this.title = loan.getBook().getTitle();
		this.isbn = loan.getBook().getIsbn();
		this.member = loan.getMember().getName();
		this.date = loan.getDate();
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getMember() {
		return member;
	}

	public Date getDate() {
		return date;
	}
}
