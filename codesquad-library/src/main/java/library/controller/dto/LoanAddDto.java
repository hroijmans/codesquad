package library.controller.dto;

public class LoanAddDto {

	private long book;
	private long member;

	public long getBook() {
		return book;
	}

	public void setBook(long book) {
		this.book = book;
	}

	public long getMember() {
		return member;
	}

	public void setMember(long member) {
		this.member = member;
	}
}
