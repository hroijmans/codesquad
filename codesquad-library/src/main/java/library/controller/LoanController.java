package library.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import library.controller.dto.LoanAddDto;
import library.controller.dto.LoanDto;
import library.model.Loan;
import library.repository.LoanRepository;
import library.repository.exception.LoanNotFoundException;
import library.service.LoanService;

@RestController
class LoanController {

	private final LoanRepository repository;

	@Autowired
	private LoanService loanService;

	LoanController(LoanRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/loans")
	List<LoanDto> getAll() {
		return repository.findAll().stream().map(LoanDto::new).collect(Collectors.toList());
	}

	@PostMapping("/loans")
	long newLoan(@Valid @RequestBody LoanAddDto newLoan) {
		return loanService.createNew(newLoan).getId();
	}

	@GetMapping("/loans/{id}")
	Loan getLoan(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new LoanNotFoundException(id));
	}

	@DeleteMapping("/loans/{id}")
	void deleteLoan(@PathVariable Long id) {
		repository.deleteById(id);
	}
}