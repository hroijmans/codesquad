package library.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import library.model.Member;
import library.repository.MemberRepository;
import library.repository.exception.MemberNotFoundException;

@RestController
class MemberController {

	private final MemberRepository repository;

	MemberController(MemberRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/members")
	List<Member> getAll() {
		return repository.findAll();
	}

	@PostMapping("/members")
	Member newMember(@Valid @RequestBody Member newMember) {
		return repository.save(newMember);
	}

	@GetMapping("/members/{id}")
	Member getMember(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new MemberNotFoundException(id));
	}

	@DeleteMapping("/members/{id}")
	void deleteMember(@PathVariable Long id) {
		repository.deleteById(id);
	}
}