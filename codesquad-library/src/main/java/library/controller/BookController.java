package library.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import library.model.Book;
import library.repository.BookRepository;
import library.repository.exception.BookNotFoundException;
import library.service.BookService;

@RestController
class BookController {

	private final BookRepository repository;

	@Autowired
	private BookService bookService;

	BookController(BookRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/books")
	List<Book> getAll() {
		return repository.findAll();
	}

	@PostMapping("/books")
	Book newBook(@Valid @RequestBody Book newBook) {
		return repository.save(newBook);
	}

	@GetMapping("/books/{id}")
	Book getBook(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
	}

	@DeleteMapping("/books/{id}")
	void deleteBook(@PathVariable Long id) {
		bookService.deleteBook(id);
	}
}