
$(document).ready(function() {
	if (currentPage() === 'books') {
		$("#books").show();
	}
	if (currentPage() === 'members') {
		$("#members").show();
	}
	if (currentPage() === 'loans') {
		$("#loans").show();
	}
});

function currentPage() {
	let searchParams = new URLSearchParams(window.location.search)
	return searchParams.get('page');
}
