
$(document).ready(function() {
	// post formdata to url specified in form action
	$('form').submit(function(e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');
		var data = form.serializeFormJSON();
		console.log(data);

		$.ajax({
			url: url,
			type: "POST",
			data: data,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				location.reload(0)
			},
			error: function(data) {
				$.each(data.responseJSON, function(key, value) {
					$(".error").append("<p>" + value + "</p>");
				});
			}
		});

	});
});

// serialize form data to json
(function($) {
	$.fn.serializeFormJSON = function() {
		var serialized = {};
		var elementArray = this.serializeArray();
		$.each(elementArray, function() {
			if (serialized[this.name]) {
				if (!serialized[this.name].push) {
					serialized[this.name] = [serialized[this.name]];
				}
				serialized[this.name].push(this.value || '');
			} else {
				serialized[this.name] = this.value || '';
			}
		});
		return JSON.stringify(serialized);
	};
})(jQuery);

